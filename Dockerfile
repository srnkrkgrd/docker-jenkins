FROM ubuntu

RUN sudo apt-get update


RUN sudo apt-get install -y python-software-properties #--fix-missing

RUN sudo add-apt-repository -y ppa:git-core/ppa
RUN sudo add-apt-repository -y ppa:webupd8team/java

RUN sudo apt-get update

RUN echo debconf shared/accepted-oracle-license-v1-1 select true | \sudo debconf-set-selections
RUN echo debconf shared/accepted-oracle-license-v1-1 seen true | \sudo debconf-set-selections

RUN sudo apt-get install -y oracle-java7-installer

RUN apt-get install -y git

RUN apt-get install -y maven

RUN wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
RUN sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
RUN sudo apt-get update
RUN sudo apt-get install -y jenkins

EXPOSE 8080

ENTRYPOINT java -jar /usr/share/jenkins/jenkins.war