Playing with docker. 

Made a Dockerfile that installs
 - newest version of Git
 - newest version of Oracle Java 7
 - newest version of Maven 3
 - newest version of Jenkins
 - expose port 8080
 - starts up Jenkins

To build: sudo docker build -t srnkrkgrd/jenkins . 
To run: sudo docker run -p 8080:8080 -d srnkrkgrd/jenkins